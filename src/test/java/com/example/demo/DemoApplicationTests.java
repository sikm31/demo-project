package com.example.demo;

import com.example.demo.controller.HelloController;
import com.example.demo.dao.ContactDao;
import com.example.demo.model.Contact;
import com.example.demo.service.ContactService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.http.MediaType;

import java.nio.charset.Charset;
import java.util.*;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.core.Is.is;

@RunWith(MockitoJUnitRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DemoApplicationTests {

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(),
			Charset.forName("utf8"));

	private MockMvc mvc;

	@Mock
	private ContactService contactService;

	@MockBean
	private ContactDao contactDao;

	@InjectMocks
	private HelloController helloController;


	List<Contact> listContactExample = Arrays.asList(new Contact(1008L,"tst3"),
			new Contact(1009L,"tst4"),new Contact(1010L,"tst5"),new Contact(1011L,"tst6"),
			new Contact(1012L,"tst7"),new Contact(1013L,"tst8"),new Contact(1014L,"tst9"));

	String exampleListJson = "[{id:1008,name:tst3},{id:1009,name:tst4},{id:1010,name:tst5}," +
			"{id:1011,name:tst6},{id:1012,name:tst7},{id:1013,name:tst8}," +
			"{id:1014,name:tst9}]";

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		mvc = MockMvcBuilders.standaloneSetup(helloController).build();

	}


	@Test
	public void test_controller() throws Exception {

		mvc.perform(get("/hello/contacts/all"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
	}

	@Test
	public void test_get_by_id_fail_404_not_found() throws Exception {

	}

	@Test
	public void test_get_by_service_conntroller() throws Exception {
		given(contactService.getSearchCondition("^.*[aei].*$")).willReturn(listContactExample);
		MockHttpServletResponse response = mvc.perform(get("/hello/contacts/all").accept(MediaType.APPLICATION_JSON))
				.andReturn().getResponse();
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
	}

	@Test
	public void test_get_by_service(){
	}


}
