package com.example.demo;


import com.example.demo.controller.HelloController;
import com.example.demo.model.Contact;
import com.example.demo.service.ContactService;
import com.example.demo.service.ContactServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class HelloRestControllerTest {

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(),
			Charset.forName("utf8"));

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ContactService contactService;



	@Before
	public void setup () {

	}
	List<Contact> listContactExample = Arrays.asList(new Contact(1008L,"tst3"),
			new Contact(1009L,"tst4"),new Contact(1010L,"tst5"),new Contact(1011L,"tst6"),
					new Contact(1012L,"tst7"),new Contact(1013L,"tst8"),new Contact(1014L,"tst9"));

	String exampleListJson = "[{id:1008,name:tst3},{id:1009,name:tst4},{id:1010,name:tst5}," +
			"{id:1011,name:tst6},{id:1012,name:tst7},{id:1013,name:tst8}," +
			"{id:1014,name:tst9}]";

	@Test
	public void contactServiceTest(){
		Mockito.when(contactService.getSearchCondition(Mockito.anyString())).thenReturn(listContactExample);
	}

	@Test
	public void contactJsonTest() throws Exception {
	}

	@Test
	public void exampleTest() throws Exception {
		this.mockMvc.perform(MockMvcRequestBuilders.get("/hello/contacts"))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

	}
	@Test
	public void exampleTest2() throws Exception {
		this.mockMvc.perform(MockMvcRequestBuilders.get("/hello/contacts?nameFilter=^.*[aei].*$")).
				andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

	}
	@Test
	public void exampleTest3() throws Exception {
		Assert.assertEquals(contactService.getSearchCondition("^.*[aei].*$"),listContactExample);
	}



}
