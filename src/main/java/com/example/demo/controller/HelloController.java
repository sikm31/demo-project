package com.example.demo.controller;

import com.example.demo.dao.ContactDao;
import com.example.demo.model.Contact;
import com.example.demo.service.ContactService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/hello/contacts")
public class HelloController {

	private static final Logger logger = Logger.getLogger(HelloController.class);

	@Autowired
	private ContactService contactService;

	@RequestMapping(method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<Contact>> getContactsWithParameter(@RequestParam(value = "nameFilter",
			required = false) String nameFilter) {
		List<Contact> listSearch = contactService.getSearchCondition(nameFilter);
		if (nameFilter.isEmpty() || listSearch.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(listSearch, HttpStatus.OK);
	}

	@RequestMapping(value = "/search",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<Contact>> getContacts(@RequestParam(value = "nameFilter",
			required = false) String nameFilter) {
		List<Contact> listSearch = contactService.getSearch(nameFilter);
		if (nameFilter.isEmpty() || listSearch.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(listSearch, HttpStatus.OK);
	}

	@RequestMapping(value = "/all",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<Contact> getIndex(){
		List<Contact> listSearch = contactService.getAll();
		return listSearch;
	}

}
