package com.example.demo.model;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

@Entity
@Table(name = "contact", indexes = {
		@Index(columnList = "name", name = "current_name_idx"),})
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "entityCache")
public class Contact {



    public Contact(Long id,String name) {
        this.id = id;
        this.name = name;
    }

    public Contact() {
    }

    @Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
