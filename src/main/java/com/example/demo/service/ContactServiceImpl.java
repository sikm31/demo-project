package com.example.demo.service;

import com.example.demo.dao.ContactDao;
import com.example.demo.model.Contact;
import org.hibernate.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class ContactServiceImpl implements ContactService {

	@Autowired
	private SessionFactory sessionFactory;


	@Autowired
	private ContactDao contactDao;

	@Override
	public List<Contact> getAll() {
		return contactDao.getAll();
	}

	@Override
	public List<Contact> getSearch(String searchName) {
		return contactDao.getSearch(searchName);
	}

	@Override
	public List<Contact> getSearchCondition(String searchName) {
		return contactDao.getSearchCondition(searchName);
	}


}
