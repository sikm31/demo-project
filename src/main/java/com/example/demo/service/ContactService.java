package com.example.demo.service;

import com.example.demo.model.Contact;

import java.util.List;

public interface ContactService {
    List<Contact> getAll();

    List<Contact> getSearch(String searchName);
    List<Contact> getSearchCondition(String searchName);
}
